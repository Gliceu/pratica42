
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Gliceu
 */
public class Elipse implements FiguraComEixos{
    
    public double eixoMaior;
    public double eixoMenor;

    public Elipse(double eixoMaior, double eixoMenor) {
        this.eixoMaior = eixoMaior;
        this.eixoMenor = eixoMenor;
    }
     public Elipse(){}
    

    @Override
    public double getEixoMenor() {
       
        return eixoMenor;
    }

    @Override
    public double getEixoMaior() {
        return eixoMaior;
    }

    @Override
    public String getNome() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getPerimetro() {
  double periElipse;
        periElipse = Math.PI * (3 * (eixoMaior + eixoMenor) - Math.sqrt((3 * eixoMaior + eixoMenor) * (eixoMaior + 3 * eixoMenor)));
        return periElipse;    }

    @Override
    public double getArea() {
         double areaElipse;
        areaElipse = Math.PI * eixoMaior * eixoMenor;
        return areaElipse;
    }
    
}
