
package utfpr.ct.dainf.if62c.pratica;
/**
 * Programação em Java.
 * @author Gliceu Camargo <gliceu@utfpr.edu.br>
 */
public interface Figura {
    public String getNome();
    public double getPerimetro();
    public double getArea();
}