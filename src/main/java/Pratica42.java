
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Gliceu Camargo <gliceu@utfpr.edu.br>
 */
public class Pratica42 {
    public static void main(String[] args) {
        Elipse elipse   =  new Elipse(10,10);
        Circulo circulo = new Circulo(10);
        
        System.out.println("** "+ elipse.getArea());
         System.out.println("***"+ elipse.getPerimetro());
          System.out.println("** "+ circulo.getArea());
           System.out.println("***"+ circulo.getPerimetro());
        
      
    }
}
